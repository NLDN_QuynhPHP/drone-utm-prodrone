<html>
<head>
	<title>Faker</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<style type="text/css">
		#debugPane {
            width: 1000px;
            height: 500px;
            border: 1px gray solid;
            overflow: auto;
            display: block;
        }
	</style>
</head>
<body>
	<script type="text/javascript">
		var flightId;
		var datelistId;
		var token;

		let local_host = "http://control.drone.dev";
		let dev_host = "http://control-dev.terra-utm.com";
		let prod_host = "https://control.terra-utm.com";
		let lg_host = "http://lg-control.terra-utm.com";
		let prodrone_host = "http://control-prodrone.terra-utm.com";
		let xmlGenerator = "xmlGenerator.php";
		let uploadKMLURL = "uploadKMLFile.php";
		let controlPRODRONE = "controlProDrone.php";
		//let KDDI_URL = "https://if-kddi.utm-drone.net";
		let KDDI_URL = "https://lgup.utm-drone.net";

		let host = lg_host;

		var socket = io(host+':3000');
		var roomID = null;

		var request_room;

		var current_possition;
		var homePosition = {};
		var loopSendLog;
		var isPaused = false;
		var KML_File;

		var PDID;

		socket.on('connect_error', function() {
		    //Process alert when can't connect
		});
		$(window).on('beforeunload', function(){
			socket.emit('LEAVE_ROOM', datelistId);
		});
		function Log(msg,divName){var msgHtml="<p><u>Time "+Date.now()+"</u>: "+msg+"</p>";divName.insertAdjacentHTML("afterbegin",msgHtml)}
	</script>
	<div>
		<div>
			<h1>PRODRONE EXAMPLE CONTROLLER</h1>
    		Flight ID: <input type="text" id="flightId" value="8"><br><br>
    		PRODRONE ID: <input type="text" id="prodroneId" value="PD1000520"><br><br>
    		<input style="width: 1000px;" type="hidden" id="token" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9sZy1jb250cm9sLnRlcnJhLXV0bS5jb20vYXBpL2xvZ2luIiwiaWF0IjoxNTA0NTM5NDAyLCJleHAiOjQ2NTgxMzk0MDIsIm5iZiI6MTUwNDUzOTQwMiwianRpIjoiRHdKcEN2dlNsUnU1WDVQOSJ9.LK6VmpNstLOBQWZrlyNWxITlHxqPsbrd5F0hkiN1RYo">
    		Home Position Latitude: <input type="text" id="homePositionLat" value="37.3126279"> | Home Position Longitude: <input type="text" id="homePositionLng" value="128.1866741"><br>
    		<br>
    		Step 1:
    		<button onclick="joinroom();">Join Room</button> <button onclick="disconnect();">Disconnect with application</button><br>
    		<br>
    		Step 2:
    		<button onclick="sethome();">Set HomePosition</button><br>
    		<br>
    		Step 3:
			<button onclick="prepare();">Prepare Mission</button> <button onclick="uploadKMLFile();">Upload KML File</button><br>
			<br>
			Step 4:
			<button onclick="start();">Power On Drone</button>
			<br><br>
			Step 5:
			<button onclick="sendlog2();">Sendlog</button> | CMD AutoStart: <button onclick="autostart();">Start Mission</button>
			<br><br>
			Step 6:
			<button onclick="pause();">Pause Drone</button>
			<button onclick="resume();">Resume Drone</button>
			<button onclick="stop();">Stop Drone</button>
			<button onclick="landed();">Land Drone</button>
			<br><br>
		</div>

		<div id="debugPane"></div>
	</div>
	<script type="text/javascript">
		function joinroom(){
			flightId = document.getElementById("flightId").value;
			token = document.getElementById("token").value;
			homePosition.lat = parseFloat(document.getElementById("homePositionLat").value);
			homePosition.lng = parseFloat(document.getElementById("homePositionLng").value);
			PDID = document.getElementById("prodroneId").value;

			request_room = {
			    'flight_id'     : flightId,
			    'type'          : 'app'
			};

			current_possition = {
				'flight_id'		: flightId,
				'lat'			: homePosition.lat,
			    'lng'			: homePosition.lng,
			    'direction'		: 0,
			    'height'		: 20,
			    'flight_time'	: 2,
			    'token'			: token
			}

			socket.emit('REQUEST_ROOM', request_room);

			socket.on('REQUEST_ROOM_SUCCEED_APP', function(flightIDResponse){
				console.log(flightIDResponse);
				if(flightId == flightIDResponse){
					socket.on('GET_FLIGHT_DETAIL_INFO_APP', function(flightData){
						Log('Request Room Succeed for Flight: '+flightId, debugPane);
						Log('Flight Data: '+JSON.stringify(flightData), debugPane);

						//TODO Generate .XML File
						var _url = xmlGenerator;
						$.ajax({
					        url: _url,
					        type: "POST",
					        contentType: "application/json",
					        data: JSON.stringify(flightData),
					        success: xmlGenerateSucceed
					    });
					});
				}
			});
		}
		function xmlGenerateSucceed(data){
			var resultGenerateFile = JSON.parse(data);
			if(resultGenerateFile.status){
				KML_File = '/tmp/'+resultGenerateFile.data.name;
				Log('KML File Generated: /tmp/'+resultGenerateFile.data.name+' - '+resultGenerateFile.data.byte+' bytes', debugPane);
			}
			else
				Log('KML File Generated: '+resultGenerateFile.status, debugPane);
		}
		function prepare(){
			socket.emit('APP_PREPARE_FLIGHT_SUCCEED', current_possition);

			socket.on('EVENT_APP_PREPARE_FLIGHT_FAILED', function(){
				Log('Create Datelist failed for Flight: '+flightId, debugPane);
			});

			socket.on('EVENT_APP_PREPARE_FLIGHT_SUCCEED', function(datelist){
				datelistId = datelist.id;
				Log('Prepare Flight Succeed for Flight: '+flightId+' Datelist: '+datelistId, debugPane);

				socket.on('SYNC', function(dataDrones){
		    		console.log(dataDrones);
		    	});
			    socket.on('START_FLIGHT_TO_APP',function(){
			    	console.log('START_FLIGHT_TO_APP');
			    });
			    socket.on('CALL_GO_HOME_TO_APP',function(){
			    	console.log('CALL_GO_HOME_TO_APP');
			    });
			    socket.on('ADD_DRONE', function(drone){
			    	console.log('Add Drone');
					console.log(drone);
				});
			});
		}
		function sethome() {
			var drone = {
				'flight_id': 		flightId,
				'lat': 				current_possition.lat,
				'lng': 				current_possition.lng,
				'height': 			0,
				'token': 			token
			}
			console.log(drone);
			socket.emit('APP_SET_HOME_POSITION', drone);

			socket.on('EVENT_APP_SET_HOME_POSITION', function(homePosition){
				Log('Set Home Position Succeed for Flight: '+homePosition.flight_id+' Home Position: Latitude: '+homePosition.lat+' Longitude: '+homePosition.lng+' Altitude: '+homePosition.height, debugPane);
			});
		}
		function disconnect(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DISCONNECTED_WITH_DRONE', drone);
		}
		function start(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DRONE_FLIGHT_STARTED', drone);
			Log('Drone Start Fly: '+flightId+' Datelist: '+datelistId, debugPane);
			//TODO Call CMD to PRODRONE
			if(PDID) {
				var _url = controlPRODRONE + '?PDID=' + PDID + '&cmd=poweron';
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: cmdSucceed
			    });
			}
		}
		function autostart(){
			//TODO Call CMD to PRODRONE
			if(PDID) {
				var _url = controlPRODRONE + '?PDID=' + PDID + '&cmd=autostart';
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: cmdSucceed
			    });
			}
		}
		function pause(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DRONE_COLLISION_PAUSE', drone);
			isPaused = true;
			Log('Drone Paused: '+flightId+' Datelist: '+datelistId, debugPane);
			//TODO Call CMD to PRODRONE
			if(PDID) {
				var _url = controlPRODRONE + '?PDID=' + PDID + '&cmd=autopause';
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: cmdSucceed
			    });
			}
		}
		function resume(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DRONE_COLLISION_RESUME', drone);
			isPaused = false;
			Log('Drone Resumed: '+flightId+' Datelist: '+datelistId, debugPane);
			//TODO Call CMD to PRODRONE
			if(PDID) {
				var _url = controlPRODRONE + '?PDID=' + PDID + '&cmd=autoresume';
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: cmdSucceed
			    });
			}
		}
		function stop(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DRONE_COLLISION_STOP', drone);
			clearInterval(loopSendLog);
			Log('Drone stopped: '+flightId+' Datelist: '+datelistId, debugPane);
			//TODO Call CMD to PRODRONE
			if(PDID) {
				var _url = controlPRODRONE + '?PDID=' + PDID + '&cmd=autostop';
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: cmdSucceed
			    });
			}
		}
		function landed(){
			var drone = {
				'datelist_id': 		datelistId
			}
			socket.emit('APP_DRONE_FLIGHT_SUCCEED', drone);
			Log('Drone landed: '+flightId+' Datelist: '+datelistId, debugPane);
		}
		function uploadKMLFile(){
			if(KML_File) {
				var _url = uploadKMLURL + '?pathFile=' + KML_File;
				$.ajax({
			        url: _url,
			        type: "GET",
			        contentType: "application/json",
			        success: uploadKMLSucceed
			    });
			}
		}
		function uploadKMLSucceed(data){
			var resultGenerateFile = JSON.parse(data);
			if(resultGenerateFile.status){
				Log('KML File Upload: Succeed '+KML_File, debugPane);
			}
			else
				Log('KML File Upload: Failed ', debugPane);
		}
		function cmdSucceed(data){
			var resultGenerateFile = JSON.parse(data);
			if(resultGenerateFile.status){
				Log('Call CMD Succeed ', debugPane);
			}
			else
				Log('Call CMD Failed ', debugPane);
		}
		function sendlog2(){
			var log = {
		    	id:				PDID,
		    	datelist_id: 	datelistId,
		    	status: 		0,
		    	message: 		'OK',
		    	datetime: 		Date.now(),
		    	locate: 		{
		    		head: 			0,
		    		height: 		10,
		    		latitude: 		current_possition.lat,
		    		longitude:		current_possition.lng,
		    		speed: 			200,
		    		current_point: 	34180,
		    	},
		    	state: 			{
		    		battery: 		55,
		    		flight_time: 	2,
					plan_status:  	1,
					go_home:  		0,
		    	},
		    	comm: 			{
		    		phone_no: 		'09011112222',
		    		range_out: 		0,
		    		antenna: 		5
		    	},
		    	token: 			token,
		    };
		    if (datelistId !== undefined) {
		    	loopSendLog = setInterval(function(){
					if(!isPaused) {
						log.locate.latitude += 0.0001;
						log.state.flight_time += 1;
						log.locate.head += 10;
						Log('Sent Log: '+JSON.stringify(log), debugPane);
						socket.emit('SYNC', log);
					}
				}, 1000);
		    } else {
		    	Log('Sent Log: Failed', debugPane);
		    }
		}
	</script>

</body>
</html>
