<?php
	ini_set('display_errors', '1');
	$PDID = $_GET['PDID'];
	$cmd = $_GET['cmd'];
	$KDDI_URL = 'https://if-kddi.utm-drone.net/droneinfo.php';

	$result = new stdClass();
	
	if ($PDID !== null && $cmd) {
		$cmdURL = $KDDI_URL.'?cmd='.$cmd.'&id='.$PDID;
		$shellResult = shell_exec ("curl '".$cmdURL."'");
	    if (strpos($shellResult, '"status":00') !== false) {
		    $result->status = true;
		} else 
			$result->status = false;
	} else 
		$result->status = false;

	$resultEncoded = json_encode($result, JSON_PRETTY_PRINT);
	echo $resultEncoded;