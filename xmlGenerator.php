<?php
	ini_set('display_errors', 1);

	$data = json_decode(file_get_contents('php://input'), true);
	//TODO Fetch Flight Data
	$flightData = $data['data']['flight'];

	//TODO Define
	$waypoints = "\n";
	$controlPoints = "\n";
	$fileName = 'SXFPMission_'.$flightData['id'].'_'.time();

	if ($flightData['homeposition'] !== null) {
		$homeposition = $flightData['homeposition'];
		// $waypoints .= '        '.$homeposition['lng'].','.$homeposition['lat'].','.floor($homeposition['height']);
		// $controlPoints .= '        0,'.dechex($point['height']).','.dechex($point['velocity']).',100,0,0,A,0,0';
		// if (count($flightData['points']) > 0) {
		// 	$waypoints .= "\n";
		// 	$controlPoints .= "\n";
		// }
	}

	foreach ($flightData['points'] as $key => $point) {
		$waypoints .= '        '.$point['lng'].','.$point['lat'].','.floor($point['height']);
		// if ($key == count($flightData['points']) - 1) {
		// 	$controlPoints .= '        ffff,0,0,100,0,0,A,0,0';
		// } else {
			$controlPoints .= '        '.($key+1).','.dechex(100*$point['height']).','.dechex(100*$point['velocity']).',100,0,0,A,0,0';
		// }
		if ($key < count($flightData['points']) - 1) {
			$waypoints .= "\n";
			$controlPoints .= "\n";
		}
	}
	$waypoints .= "\n";
	$controlPoints .= "\n";

	//TODO Create DomXML
	$dom = new DOMDocument('1.0', 'utf-8');
	$dom->formatOutput = true;

	$node = $dom->createElementNS('http://www.opengis.net/kml/2.2', 'kml');
	$kmlNode = $dom->appendChild($node);

	//TODO Create Document Node
	$documentElement = $dom->createElement('Document');
	$documentNode = $kmlNode->appendChild($documentElement);

	$name = $dom->createElement('name', $fileName);
	$documentNode->appendChild($name);

	//TODO Placemark Node
	$placeMarkElement = $dom->createElement('Placemark');
	$placeMarkNode = $documentNode->appendChild($placeMarkElement);

		$nameRoute = $dom->createElement('name', $flightData['name']);
		$placeMarkNode->appendChild($nameRoute);

		//TODO LineString Node
		$lineStringElement = $dom->createElement('LineString');
		$lineStringNode = $placeMarkNode->appendChild($lineStringElement);

			$tessellateElement = $dom->createElement('tessellate', 1);
			$lineStringNode->appendChild($tessellateElement);

			$coordinatesElement = $dom->createElement('coordinates', $waypoints);
			$lineStringNode = $lineStringNode->appendChild($coordinatesElement);

		//TODO ControlString Node
		$controlStringElement = $dom->createElement('ControlString');
		$controlStringNode = $placeMarkNode->appendChild($controlStringElement);

			$controlsElement = $dom->createElement('controls', $controlPoints);
			$controlsElement->setAttribute('rev', 'PD000');
			$controlStringNode->appendChild($controlsElement);

	$resultSaveDom = $dom->save("/tmp/".$fileName.".kml");
	$result = new stdClass();
	if ($resultSaveDom !== false) {
		$result->status = true;

		$fileSaved = new stdClass();
		$fileSaved->name = $fileName.'.kml';
		$fileSaved->byte = $resultSaveDom;

		$result->data = $fileSaved;
	} else {
		$result->status = false;
	}

	$resultEncoded = json_encode($result, JSON_PRETTY_PRINT);
	echo $resultEncoded;
