<?php
	ini_set('display_errors', '1');
	$pathFile = $_GET['pathFile'];
	$KDDI_URL = 'https://if-kddi.utm-drone.net/uploadkml.php';

	$result = new stdClass();

	if ($pathFile !== null) {
		$shellResult = shell_exec ("curl -X POST -F 'filename=@".$pathFile."' ".$KDDI_URL);
	    if (strpos($shellResult, 'OK,') !== false) {
		    $result->status = true;
		} else 
			$result->status = false;
	} else 
		$result->status = false;

	$resultEncoded = json_encode($result, JSON_PRETTY_PRINT);
	echo $resultEncoded;